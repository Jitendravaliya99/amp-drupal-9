<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/newsportal_amp/templates/page.html.twig */
class __TwigTemplate_f5c39154c3f4a14f4bc29266d37e27c7e8b2c6b9af526c8af6726831bca5a0c4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'featured' => [$this, 'block_featured'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("block" => 44, "if" => 75);
        $filters = array("escape" => 78);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['block', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "<div id=\"page-wrapper\">
  <div id=\"page\">
    <header class=\"header\">
      ";
        // line 44
        $this->displayBlock('head', $context, $blocks);
        // line 74
        echo "    </header>
    ";
        // line 75
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 75)) {
            // line 76
            echo "      <div class=\"highlighted\">
        <aside class=\"container section\">
          ";
            // line 78
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 78), 78, $this->source), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 82
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_top", [], "any", false, false, true, 82)) {
            // line 83
            echo "      ";
            $this->displayBlock('featured', $context, $blocks);
            // line 90
            echo "    ";
        }
        // line 91
        echo "    <div class=\"main-wrapper\">
      ";
        // line 92
        $this->displayBlock('content', $context, $blocks);
        // line 126
        echo "    </div>
    ";
        // line 127
        if (((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_bottom_first", [], "any", false, false, true, 127) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_bottom_second", [], "any", false, false, true, 127)) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_bottom_third", [], "any", false, false, true, 127))) {
            // line 128
            echo "      <div class=\"featured-bottom\">
        <aside class=\"container\">
          ";
            // line 130
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_bottom_first", [], "any", false, false, true, 130), 130, $this->source), "html", null, true);
            echo "
          ";
            // line 131
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_bottom_second", [], "any", false, false, true, 131), 131, $this->source), "html", null, true);
            echo "
          ";
            // line 132
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_bottom_third", [], "any", false, false, true, 132), 132, $this->source), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 136
        echo "    <footer class=\"site-footer\">
      ";
        // line 137
        $this->displayBlock('footer', $context, $blocks);
        // line 155
        echo "    </footer>
  </div>
</div>
";
    }

    // line 44
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "        ";
        if (((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "secondary_menu", [], "any", false, false, true, 45) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "top_header", [], "any", false, false, true, 45)) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "top_header_form", [], "any", false, false, true, 45))) {
            // line 46
            echo "          <nav";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_top_attributes"] ?? null), 46, $this->source), "html", null, true);
            echo " class=\"container\">
              ";
            // line 47
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "secondary_menu", [], "any", false, false, true, 47), 47, $this->source), "html", null, true);
            echo "
              ";
            // line 48
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "top_header", [], "any", false, false, true, 48), 48, $this->source), "html", null, true);
            echo "
              ";
            // line 49
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "top_header_form", [], "any", false, false, true, 49)) {
                // line 50
                echo "                <div class=\"form-inline navbar-form\">
                  ";
                // line 51
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "top_header_form", [], "any", false, false, true, 51), 51, $this->source), "html", null, true);
                echo "
                </div>
              ";
            }
            // line 54
            echo "          </nav>
        ";
        }
        // line 56
        echo "        <nav";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_attributes"] ?? null), 56, $this->source), "html", null, true);
        echo " class=\"container\">
            ";
        // line 57
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 57), 57, $this->source), "html", null, true);
        echo "
            ";
        // line 58
        if ((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 58) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_form", [], "any", false, false, true, 58))) {
            // line 59
            echo "              <div class=\"navbar-collapse\">
              <button class=\"amp-sidebar-btn\" type=\"button\" on=\"tap:sidebar\"><span></span></button>
                ";
            // line 61
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 61), 61, $this->source), "html", null, true);
            echo "
                ";
            // line 62
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_form", [], "any", false, false, true, 62)) {
                // line 63
                echo "                  <div class=\"form-inline navbar-form\">
                    ";
                // line 64
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_form", [], "any", false, false, true, 64), 64, $this->source), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 67
            echo "\t          </div>
            ";
        }
        // line 69
        echo "            ";
        if (($context["sidebar_collapse"] ?? null)) {
            // line 70
            echo "              <button class=\"navbar-toggler navbar-toggler-left\" type=\"button\"></button>
            ";
        }
        // line 72
        echo "        </nav>
      ";
    }

    // line 83
    public function block_featured($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 84
        echo "        <div class=\"featured-top\">
          <aside class=\"featured-top__inner container\">
            ";
        // line 86
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "featured_top", [], "any", false, false, true, 86), 86, $this->source), "html", null, true);
        echo "
          </aside>
        </div>
      ";
    }

    // line 92
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 93
        echo "        <div class=\"container\">
          ";
        // line 94
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 94), 94, $this->source), "html", null, true);
        echo "
          <main";
        // line 95
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_attributes"] ?? null), 95, $this->source), "html", null, true);
        echo "> 
            <section class=\"section\">
              ";
        // line 97
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 97), 97, $this->source), "html", null, true);
        echo "
            </section>
          </main>
          ";
        // line 100
        if ((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "homepage_content", [], "any", false, false, true, 100) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "homepage_sidebar", [], "any", false, false, true, 100))) {
            // line 101
            echo "          <main class=\"home-page\">
            ";
            // line 102
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "homepage_content", [], "any", false, false, true, 102)) {
                // line 103
                echo "              <section class=\"home-page__main\">
                ";
                // line 104
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "homepage_content", [], "any", false, false, true, 104), 104, $this->source), "html", null, true);
                echo "
              </section>
            ";
            }
            // line 107
            echo "            ";
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "homepage_sidebar", [], "any", false, false, true, 107)) {
                // line 108
                echo "              <aside class=\"home-page__sidebar\">
                ";
                // line 109
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "homepage_sidebar", [], "any", false, false, true, 109), 109, $this->source), "html", null, true);
                echo "
              </aside>
            ";
            }
            // line 112
            echo "          </main>
          ";
        }
        // line 114
        echo "          ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 114)) {
            // line 115
            echo "            <aside class=\"section\">
              ";
            // line 116
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 116), 116, $this->source), "html", null, true);
            echo "
            </aside>
          ";
        }
        // line 119
        echo "          ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 119)) {
            // line 120
            echo "            <aside class=\"section\">
              ";
            // line 121
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 121), 121, $this->source), "html", null, true);
            echo "
            </aside>
          ";
        }
        // line 124
        echo "        </div>
      ";
    }

    // line 137
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 138
        echo "        <div class=\"container\">
          ";
        // line 139
        if ((((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_first", [], "any", false, false, true, 139) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_second", [], "any", false, false, true, 139)) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_third", [], "any", false, false, true, 139)) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_fourth", [], "any", false, false, true, 139))) {
            // line 140
            echo "            <div class=\"site-footer__top clearfix\">
              ";
            // line 141
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_first", [], "any", false, false, true, 141), 141, $this->source), "html", null, true);
            echo "
              ";
            // line 142
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_second", [], "any", false, false, true, 142), 142, $this->source), "html", null, true);
            echo "
              ";
            // line 143
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_third", [], "any", false, false, true, 143), 143, $this->source), "html", null, true);
            echo "
              ";
            // line 144
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_fourth", [], "any", false, false, true, 144), 144, $this->source), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 147
        echo "          ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_fifth", [], "any", false, false, true, 147)) {
            // line 148
            echo "            <div class=\"site-footer__bottom\">
              ";
            // line 149
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_fifth", [], "any", false, false, true, 149), 149, $this->source), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 152
        echo "        </div>
        ";
        // line 153
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 153), 153, $this->source), "html", null, true);
        echo "
      ";
    }

    public function getTemplateName()
    {
        return "themes/custom/newsportal_amp/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  364 => 153,  361 => 152,  355 => 149,  352 => 148,  349 => 147,  343 => 144,  339 => 143,  335 => 142,  331 => 141,  328 => 140,  326 => 139,  323 => 138,  319 => 137,  314 => 124,  308 => 121,  305 => 120,  302 => 119,  296 => 116,  293 => 115,  290 => 114,  286 => 112,  280 => 109,  277 => 108,  274 => 107,  268 => 104,  265 => 103,  263 => 102,  260 => 101,  258 => 100,  252 => 97,  247 => 95,  243 => 94,  240 => 93,  236 => 92,  228 => 86,  224 => 84,  220 => 83,  215 => 72,  211 => 70,  208 => 69,  204 => 67,  198 => 64,  195 => 63,  193 => 62,  189 => 61,  185 => 59,  183 => 58,  179 => 57,  174 => 56,  170 => 54,  164 => 51,  161 => 50,  159 => 49,  155 => 48,  151 => 47,  146 => 46,  143 => 45,  139 => 44,  132 => 155,  130 => 137,  127 => 136,  120 => 132,  116 => 131,  112 => 130,  108 => 128,  106 => 127,  103 => 126,  101 => 92,  98 => 91,  95 => 90,  92 => 83,  89 => 82,  82 => 78,  78 => 76,  76 => 75,  73 => 74,  71 => 44,  66 => 41,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/newsportal_amp/templates/page.html.twig", "C:\\xampp\\htdocs\\amp-drupal\\themes\\custom\\newsportal_amp\\templates\\page.html.twig");
    }
}
